/**
 * Created by Ruslan Koloskov (softua6@gmail.com) on 25.11.2014.
 */
define(['jquery'], function ( $ ) {

	function Timer(date, elem)
	{
		// текущая дата
		this.currDate = new Date();
		// ожидаемая дата
		this.futureDate = new Date(date);

		this.standart = {
			second: 1000,
			minut: 60 * 1000,
			hour: 60 * 1000 * 60,
			day: 60 * 1000 * 60 * 24
		};

		this.delta = this.futureDate - this.currDate;

		var self = this;
		self.getStringTimeLeft = function() {
			var timeLeft = self.delta;
			var days, hours, minutes, seconds = '';

			// дни
			var daysCount = Math.floor(timeLeft / self.standart.day);
			if(daysCount >= 1) {
				timeLeft -= self.standart.day * daysCount;
				days = (daysCount < 10) ? '0' + daysCount + ' дней ' : daysCount + ' дней ';
			} else {
				days = '00 дней ';
			}

			// чвсы
			var hoursCount = Math.floor(timeLeft / self.standart.hour);
			if(hoursCount >= 1) {
				timeLeft -= self.standart.hour * hoursCount;
				hours = (hoursCount < 10) ? '0' + hoursCount + ' часов ' : hoursCount + ' часов ';
			} else {
				hours = '00 часов ';
			}

			// минуты
			var minutesCount = Math.floor(timeLeft / self.standart.minut);
			if(minutesCount >= 1) {
				timeLeft -= self.standart.minut * minutesCount;
				minutes = (minutesCount < 10) ? '0' + minutesCount + ' минут ' : minutesCount + ' минут ';
			}
			else {
				minutes = '00 минут';
			}

			// секунды
			var secondsCount = Math.floor(timeLeft / self.standart.second);
			if (secondsCount >= 1) {
				timeLeft -= self.standart.second * secondsCount;
				seconds = (secondsCount < 10) ? '0' + secondsCount + ' секунд' : secondsCount + ' секунд';
			}
			else {
				seconds = '00 секунд';
			}

			return days + hours + minutes + seconds;

		};
		var interval = setInterval(function() {
			if (self.delta >= 1000) {
				elem.html(self.getStringTimeLeft());
				self.delta -= 1000;
			}
			else {
				elem.html('Время вышло!');
				clearInterval(interval);
			}
		}, 1000);
	}

	return Timer;
});