/**
 * Created by Ruslan Koloskov (softua6@gmail.com)
 * Date: 28.11.2014
 * Time: 12:42
 */

define(['jquery'], function ( $ ) {
	var AddToCart = {};

	AddToCart.init = function (elems, animTime) {
		var animationTime = animTime || 800;
		elems.on('click', '.catalog__buy', function(event) {

			if ( !$(this).hasClass('added-to-cart') ) {
				event.preventDefault();
				var wind = $(window);
				var cart = $('.header__cart'); // целевой элемент
				var isCartHidden = cart.hasClass('header__cart--hidden');
				var targetTop, targetLeft; // целевое смещение по вертикали и горизонтали

				if (wind.scrollTop() >= parseInt(cart.offset().top) + parseInt(cart.height())) {
					targetTop = -5;
					targetLeft = parseInt(cart.offset().left);
				}
				else if (wind.scrollTop() == 0) {
					targetTop = parseInt(cart.offset().top);
					targetLeft = parseInt(cart.offset().left);
				}
				else {
					targetTop = parseInt(cart.offset().top - parseInt(wind.scrollTop));
					targetLeft = parseInt(cart.offset().left);
				}

				$(this).addClass('added-to-cart');

				var elem = $(this).parents('.catalog__item');
				var top = elem.offset().top;
				var left = elem.offset().left;

				var clone = elem.clone().css({
					position: 'fixed',
					top: parseInt(top - $(window).scrollTop()),
					left: parseInt(left) - parseInt($(window).scrollLeft()),
					marginLeft: 0
				}).appendTo('.catalog');

				$(this).html('Оформить');
				clone.animate({
					top: targetTop,
					left: targetLeft,
					width: 20,
					height: 20,
					opacity: 0,
					display: 'none'
				}, animationTime);

				setTimeout(function() {
					clone.remove();
					var cartItems = 0;
					if (isCartHidden) {
						cart.html(cartItems + 1);
						cart.removeClass('header__cart--hidden');
					}
					else {
						cart.html(parseInt(cart.html()) + 1);
					}
				}, animationTime);
			}
		});
	};

	// Думаю, что тут нужно реализовать модуль, который будет отправлять ajax
	// Логично, если этот модуль будет указан в зависимостях к текущему модулю

	return AddToCart;
});