/**
 * Placeholder legacy support
 *
 */
define( 'legacyplaceholder', ['jquery'], (function( $ ) {
    'use strict';

    return function( name ) {
        var className = name || 'is-placeholder',
            $elements = $( 'input, textarea' ).filter( '[placeholder]' );

        $.each( $elements, function() {
            var $this = $( this );
            $this.addClass( className );
            this.value = this.placeholder = $this.attr( 'placeholder' );
        } );

        $elements.on( {
            focusin: function() {
                var $this = $( this ),
                    placeholderText = this.placeholder;

                if ( this.value === placeholderText ) {
                    $this.removeClass( className );
                    this.value = '';
                }

                $this.one( 'focusout', {placeholder: placeholderText}, function( e ) {
                    if ( this.value === '' ) {
                        $( this ).addClass( className );
                        this.value = e.data.placeholder;
                    }
                } );
            }
        } );
    };

}) );
