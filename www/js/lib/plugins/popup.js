define( ['jquery'], function( $ ) {
    'use strict';

    var popup = {

        $el: $( '.popup' ),

        init: function() {
            if ( this.$el.length ) {
                this.$fade = {};

                $( '.js-popup' ).on( 'click.popup', this.show );
                this.$el.find('.popup__close').on( 'click.popup', this.destroy );

                this.$el.on( 'click', function( e ) {
                    e.stopPropagation();
                } );
            }
        },

        destroy: function( e ) {
            if ( typeof e !== 'undefined' && e.type === 'keyup' ) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if ( code !== 27 ) {
                    return false;
                }
            }

            popup.$el.css( 'display', 'none' );
            popup.$fade.remove();

            $( 'body' ).off( '.popup' );
        },

        show: function() {
            var $this = $( this ),
                type = $this.data( 'type' ),
                $popup = popup.$el,
                $currentPopup = $popup.filter('.popup--' + type ),
                scrollTop = window.pageYOffset || document.documentElement.scrollTop,
                offset = 50, position;

            if ( $this.data( 'disabled' ) === true ) {
                return false;
            }

            popup.$fade = $( '<div id="fade"/>' );

            $popup.css( {
                display: 'none'
            });

            position = scrollTop + offset + $currentPopup.outerHeight() > $(document ).height() ? 'bottom' : 'top';

            $currentPopup.css( {
                display: 'block',
                top: position === 'top' ? scrollTop + offset : 'auto',
                bottom: position === 'bottom' ? offset : 'auto'
            });


            $( 'body' ).on( 'keyup.popup', popup.destroy );
            popup.$fade.one( 'click.popup', popup.destroy );
            $( 'body' ).append( popup.$fade );

            return false;
        }
    };

    popup.init();
});
