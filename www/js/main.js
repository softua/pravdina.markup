polyfill(); // legacy css engine selectors

var version = {
		jquery: (window.msie < 9) ? '1.11.1' : '2.1.1',
		jqueryui: '1.11.1',
		underscore: '1.6.0',
		backbone: '1.1.2'
	},
	isDev = window.location.hostname.search('.dev') > -1;

require.config({
//    urlArgs: 'v=' + (isDev ? Math.random() : 1),
	baseUrl: '/js/lib',
	paths: {
		jquery: [
			isDev ? 'jquery-' + version.jquery + '.min'
				: '//yandex.st/jquery/' + version.jquery + '/jquery.min',
			'jquery-' + version.jquery + '.min'
		],
		popup: 'plugins/popup',
		myslider: 'plugins/jquery.myslider',
		legacyplaceholder: 'plugins/legacyplaceholder',
		scrollbar: 'plugins/perfect-scrollbar',
		grayscale: 'plugins/grayscale',
		timer: 'plugins/timer',
		addToCart: 'plugins/add-to-cart'
	},
	shim: {}
});

// common
require(['jquery', 'popup']);

// mySlider
if (document.getElementsByClassName('js-slider').length) {
	require(['jquery', 'myslider'], function ($) {
		'use strict';
		$('.slider-outer').mySlider({
			autoPlay: true,
			autoPlayDuration: 8000,
			slider: '.slider',
			sliderItem: '.slider__item',
			arrowsAppend: false,
			switcher: '.slider-outer__switcher',
			switcherItems: '.slider-outer__switcher__item',
			switcherAppend: false
		});

		$('.sidebar__slider-outer').mySlider({
			autoPlay: true,
			autoPlayDuration: 5000,
			slider: '.sidebar__slider',
			sliderItem: '.sidebar__slider__item',
			switcher: '.sidebar__slider-outer__switcher',
			switcherItems: '.sidebar__slider-outer__switcher__item'
		});
	});
}

// if the placeholder attribute is not supported
if (!('placeholder' in document.createElement('input'))) {
	require(['legacyplaceholder'], function (placeholder) {
		'use strict';
		placeholder(/* className for placeholder, by default 'is-placeholder' */);
	});
}

function polyfill() {
	'use strict';

	/* jshint ignore:start */
	if (!document.querySelectorAll) {
		(function (d, s) {
			d = document, s = d.createStyleSheet();
			d.querySelectorAll = function (r, c, i, j, a) {
				a = d.all, c = [], r = r.replace(/\[for\b/gi, '[htmlFor').split(',');
				for (i = r.length; i--;) {
					s.addRule(r[i], 'k:v');
					for (j = a.length; j--;) a[j].currentStyle.k && c.push(a[j]);
					s.removeRule(0);
				}
				return c;
			};
		})();
	}
	/*jshint ignore:end */

	if (!document.querySelector) {
		document.querySelector = function (selectors) {
			var elements = document.querySelectorAll(selectors);
			return (elements.length) ? elements[0] : null;
		};
	}

	if (!document.getElementsByClassName) {
		document.getElementsByClassName = function (match) {
			var result = [],
				elements = document.getElementsByTagName('*'),
				i, elem;
			match = ' ' + match + ' ';

			for (i = elements.length; i--;) {
				elem = elements[i];
				if ((' ' + (elem.className || elem.getAttribute('class')) + ' ').indexOf(match) > -1) {
					result.push(elem);
				}
			}
			return result;
		};
	}
}

// скроллинг
require(['jquery'], function ($) {
	var listWrapper = $('[data-scrollable="true"]');
	if (listWrapper) {
		var list = listWrapper.find('.teachers__list');
		var elements = list.children();
		var width = 0;
		elements.each(function (ind, elem) {
			width += parseInt($(elem).outerWidth(true))
		});
		list.css({width: width});

		require(['scrollbar'], function () {
			listWrapper.perfectScrollbar({
				suppressScrollY: true
			});
		});
	}
});

// цветность картинок
require(['jquery'], function ( $ ) {
	var images = $('[data-grayscale="true"]');

	if (images.length) {
		require(['grayscale'], function(grayscale) {
			grayscale(images);

			images.on('mouseover', function(event) {
				$(event.target).css({opacity: 0.5});
				setTimeout(function(){
					grayscale.reset(event.target);
					$(event.target).css({opacity: 1});
				}, 300);
			});
			images.on('mouseleave', function (event) {
				$(event.target).css({opacity: 0.5});
				setTimeout(function(){
					grayscale(event.target);
					$(event.target).css({opacity: 1});
				}, 300);
			});
		});
	}
});

// Этот код убираем, чтобы отменить таймер
require(['jquery', 'timer'], function ( $, Timer ) {
	Timer(Date.UTC(2014, 11, 3, 7, 0, 0), $('.main__title'));
});

// добавление элементов в корзину
require(['jquery'], function($) {
	var elems = $('.catalog__item');
	if (elems.length) {
		require( ['addToCart'], function( AddToCart ) {
			AddToCart.init( elems );
		});
	}
});